﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace First_ASP_App.Controllers
{
    public class StoreController : Controller
    {
        // GET: Store
        public string Index()
        {
            return "Hello Store Index!";
        }

        public string Browse(string genre)
        {
            string message = HttpUtility.HtmlEncode("Store.Browse , Genre = " + genre);
            return message;
        }

        public string Details(int id)
        {
            return "Hello Store Details ID = " + id;
        }
    }
}